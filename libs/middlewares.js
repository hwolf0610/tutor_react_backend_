let jwt  = require('jsonwebtoken');

// models

let User            = require('@models/users');

let middleware = {};

middleware.isLoggedIn = function(req, res, next){

        // check header or url parameters or post parameters for token
         var token = req.body.access_token || req.query.access_token || req.headers['x-access-token'];
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, process.env.SESSION_SECRET, function(err, decoded) {
                if (err) {
                    return res.status(401).send({
                        success: false,
                        message: 'Sign in to continue.',
                        error: err.toString()
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    let user = decoded;

                    //check to see if this user exist in the system, if not tell the person to relogin
                    User.findById(user.id, function(err, usr){
                      if(!usr){
                        console.log(err);
                        return res.status(401).send({
                            success: false,
                            message: 'Re Sign in to continue.'
                        });
                      }
                      res.locals.user = usr;
                      next();
                    });
                }
            });
        } else {
            // if there is no token
            // return an error
            console.log(" there is an err");

            return res.status(401).send({
                success: false,
                message: 'Sign in to continue.'
            });
        }

}

middleware.isUserOrAdmin = function(req, res, next){

    if(req.body.user_id == res.locals.user.id || res.locals.user.role == 'admin' || res.locals.user.role == 'owner' || res.locals.user.email == 'christopher@expanse.tech' ) {
      next();
    }else{
      console.log(err);
      return res.status(401).send({
          success: false,
          message: 'Sign in to continue.'
      });
    }

}

middleware.isAdmin = function(req, res, next){

    if(res.locals.user.role == 'admin' || res.locals.user.role == 'owner' || res.locals.user.email == "christopher@expanse.tech" ) {
      next();
    }else{
      console.log(err);
      return res.status(401).send({
          success: false,
          message: 'Sign in to continue.'
      });
    }

}



module.exports = middleware;
