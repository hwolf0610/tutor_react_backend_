let Exports = {}

//USER CONTROLLERS
Exports.UserController          = require('./user');
//AUTH CONTROLLERS
Exports.AuthController          = require('./auth');

module.exports = Exports;
