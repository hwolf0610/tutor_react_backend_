let User = require('@models/users');
let dotenv = require('dotenv');
const nodemailer = require('nodemailer');
const crypto = require('crypto')


// TODO: Change "secret" to a private key
// https://www.npmjs.com/package/jsonwebtoken
let jwt  = require('jsonwebtoken');

let controller = {}

controller.create = function(req, res){
  User.find({
          email: req.body.email
      }).limit(1)
      .then(function(result) {

          if (result.length) {
              res.status(200).json({
                  message: "User already exists."
              })
          } else {
              let email = req.body.email;
              let password = req.body.password;

              req.checkBody('email', 'Email is required').notEmpty();
              req.checkBody('email', 'Email is not valid').isEmail();
              //req.checkBody('username', 'Username is required').notEmpty()
              req.checkBody('password', 'Password is required').notEmpty();
              req.checkBody('password', 'Password isnt long enough').isLength(5);
              //req.checkBody('password2', 'Passwords do not match').equals(req.body.password)

              var errors = req.validationErrors();

              if (errors) {
                  res.status(400).json({
                      message: "failed",
                      errors: errors
                  })
              } else {
                  //create new user
                  let Usr = new User({
                      email: email,
                      password: password,
                      role: 'user',
                      firstname: req.body.firstname,
                      lastname: req.body.lastname,
                      is_active:false
                  });
                  let transport = nodemailer.createTransport({
                      host: 'smtp.gmail.com',
                      port: 587,
                      auth: {
                          user: 'testproject628@gmail.com',
                          pass: 'hello1234@'
                      }
                  });
                  

                  User.createUser(Usr, function(err, user) {
                      if (err) throw err
                      //console.log(user);

                      let token = jwt.sign({ id: user._id }, process.env.SESSION_SECRET, {
                        expiresIn: 86400 // expires in 24 hours
                      });
                      
                      const message = {
                          from: 'testproject628@gmail.com', // Sender address
                          to: user.email, // List of recipients
                          subject: 'Registration', // Subject line
                          text: 'successfully Signup link here for verfication http://localhost:3000/api/auth/verification/'+token // Plain text body
                      };
                      //send email
                      transport.sendMail(message, function (err, info) {
                          if (err) {
                              console.log(err)
                          } else {
                              console.log(info);
                          }
                      });

                      res.status(200).json({
                          message: "User created",
                          user: user,
                          access_token: token
                      });
                  });
              }
          }
      })
      .catch(function(err) {
          if (err) {
              console.log(err);
          }
      });
}
controller.verification = function (req, res){
    const usertoken = req.params.verification_token
    console.log(usertoken)
    jwt.verify(usertoken, process.env.SESSION_SECRET, function (err, decoded) {
        if(err){
            return res.status(400).json({
                message: "Token Invalid"
            })
        }else{
            console.log(decoded.id)
            try{
                let usr_id = decoded.id
                User.getUserById(usr_id, function (err, user) {
                    user.is_active=true
                    user.save()
                });
                return res.status(200).json({
                    message: "Verification Successfully",
                })
            }catch(err){
                return res.status(400).json({
                    message: "Something Went Wrong",
                    error:err
                })
            }
        }
    });
}

controller.login = function(req, res){
  User.getUserByEmail(req.body.email, function(err, user) {
      if (err) throw err
      if (!user) {
          return res.status(400).json({
            message: "email doesnt exist."
          })
      }
      User.comparePassword(req.body.password, user.password, function(err, isMatch) {
          if (err) throw err;
          if (isMatch) {

            let token = jwt.sign({ id: user._id, email: user.email, role: user.role }, process.env.SESSION_SECRET, {
              expiresIn: process.env.SESSION_TTL // expires in 24 hours
            });

              return res.status(200).json({
                message: "Login Success",
                access_token: token,
                user: { id: user._id, email: user.email, role: user.role }
              })
          } else {
              return res.status(400).json({
                message: "password doesnt match."
              })
          }
      })
  })
}

module.exports = controller;
