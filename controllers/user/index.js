let dotenv = require('dotenv');
let jwt  = require('jsonwebtoken');
const nodemailer = require('nodemailer');
let User = require('@models/users');

let Controller = {};

Controller.ViewUser = function(req, res){
  User.findById(req.params.user_id, '-password')
  .populate('badges')
  .exec()
  .then(function(user) {
      if(user){
        res.status(200).json({
            message: "User Found",
            user: user
        });
      }else{
        console.log(err);
        res.status(400).json({
            message: "User doesnt exist"
        });
      }
  })
  .catch(function(err){
    console.log(err)
    res.status(400).json({
        message: "User doesnt exist"
    });
  })
}

Controller.ForgetPassword = function (req, res) {
    let usr_email = req.body.email
    let transport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        auth: {
            user: 'testproject628@gmail.com',
            pass: 'hello1234@'
        }
    });
    User.getUserByEmail(usr_email, function (err, user) {
        if (err) throw err;
        try{
            let token = jwt.sign({
                id: user._id
            }, process.env.SESSION_SECRET, {
                expiresIn: 86400 // expires in 24 hours
            });
            const message = {
                from: 'testproject628@gmail.com', // Sender address
                to: user.email, // List of recipients
                subject: 'Forget Password', // Subject line
                text: 'For Password Change click here http://localhost:3000/api/user/password/update/' + token // Plain text body
            };

            //send email
            transport.sendMail(message, function (err, info) {
                if (err) {
                    console.log(err)
                } else {
                    console.log("email send successfully to " + user.email);
                }
            });
            return res.status(200).json({
                message: "Forget request send to your email",
            })
        }catch(err){
            return res.status(400).json({
                message: "Something Went Wrong",
                error: err
            })
        }
        
    });
}

Controller.PasswordUpdate = function(req, res){
    //check to see if user exist
    //update users password to new password
    //save user
    //return json
    let usr_id = req.body.user_id;
    User.getUserById(usr_id, function(err, user) {

        User.comparePassword(req.body.old_password, user.password, function(err, isMatch) {
            if (err) throw err;

            if (isMatch) {
                user.password = req.body.new_password;
                User.updatePassword(user, function(err, usr) {
                    res.status(200).json({
                        message: "User Password Updated",
                        user: usr
                    });
                });
            }else{
            console.log(err);
            res.status(400).json({
                message: "Old password does not match"
            });
            }
        })
    });

}

Controller.AddProfile = async function (req, res) {
    let user_id = req.body.user_id;
    User.getUserById(user_id, function (err, user) {
        if (err) throw err
        if (!user) {
            return res.status(400).json({
                message: "user not exist."
            })
        }
        try {
            if (req.body.firstname) {
                user.firstname = req.body.firstname
            }
            if (req.body.lastname) {
                user.lastname = req.body.lastname
            }
            
            user.save()
            return res.status(200).json({
                message: "User Profile Update Successfully",
            })
        } catch (err) {
            console.log(err)
            return res.status(400).json({
                message: "Something went wrong!",
                error: err.toString()
            })
        }

    });


}

module.exports = Controller;
