let Exports = {};

Exports.indexRouter        = require('./index');
Exports.apiRouter          = require('./api/index');
Exports.apiAuth            = require('./api/auth');
Exports.apiUser            = require('./api/user');



module.exports = Exports;
