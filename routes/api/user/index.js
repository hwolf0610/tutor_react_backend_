let express = require('express');
let router = express.Router();

let mw = require('@middlewares');

let {UserController} = require('@controllers');

/* POST */

/**
 * Get a user object.
 * @access     authenticated
 *
 * @param {string}  x-access-token    Access token is aquired at login.
 * @param {string}  user_id           The users id.
 * @return {object.json} A user object with out the users password.
 */
router.get('/view/:user_id',  UserController.ViewUser);
/**
 * Forget Password
 * @access     authenticated
 *
 * @param {string}  email      The users email
 */
router.post('/password/forgot_password', UserController.ForgetPassword);
/**
 * Change password
 * @access     authenticated
 *
 * @param {string}  x-access-token    Access token is aquired at login.
 * @param {string}  old_password      The users old password.
 * @param {string}  new_password      The users new password.
 * @param {string}  user_id           optional user_id
 * @return {object.json} A user object with out the users password.
 */
router.post('/password/update',UserController.PasswordUpdate);

router.post('/profile/add',  UserController.AddProfile);

module.exports = router;
