let express = require('express');
let router = express.Router();

let {AuthController} = require('@controllers');

/* POST */
/**
 * Creates a new user.
 * @access     public
 *
 * @param {string}  email     The users email.
 * @param {string}  password  the users password.
 * @return {object.json} Either returns an error or some json.
 */
router.post('/create', AuthController.create);

/**
 * Log in a user.
 * @access     public
 *
 * @param {string}  email     The users email.
 * @param {string}  password  the users password.
 * @return {object.json} Returns user object and access key.
 */
// Login User
router.post('/login', AuthController.login);

// registration email verification 
router.get('/verification/:verification_token', AuthController.verification);

module.exports = router;
